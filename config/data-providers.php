<?php

return [
    'DataProviderW' => [
        'database_path' => '/json/DataProviderW.json',
        'mappings' => [
            'amount' => 'amount',
            'currency' => 'currency',
            'phone' => 'phone',
            'status' => 'status',
            'codes' => [
                'paid' => 'paid',
                'pending' => 'pending',
                'reject' => 'reject'
            ],
            'created_at' => 'created_at',
            'unique_id' => 'id'
        ]
    ],
    'DataProviderX' => [
        'database_path' => '/json/DataProviderX.json',
        'mappings' => [
            'amount' => 'transactionAmount',
            'currency' => 'Currency',
            'phone' => 'senderPhone',
            'status' => 'transactionStatus',
            'codes' => [
                'paid' => 1,
                'pending' => 2,
                'reject' => 3
            ],
            'created_at' => 'transactionDate',
            'unique_id' => 'transactionIdentification'
        ]
    ],
    'DataProviderY' => [
        'database_path' => '/json/DataProviderY.json',
        'mappings' => [
            'amount' => 'amount',
            'currency' => 'currency',
            'phone' => 'phone',
            'status' => 'status',
            'codes' => [
                'paid' => 100,
                'pending' => 200,
                'reject' => 300
            ],
            'created_at' => 'created_at',
            'unique_id' => 'id'
        ]
    ],
];
