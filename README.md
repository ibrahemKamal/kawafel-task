### Requirements:
    - PHP >= 8.0
    - Composer

### Installation & Usage
    - run `composer install`
    - for testing: run `vendor/bin/phpunit`

### How the project is structured?
    - The core part of the project is the `config/data-providers.php` file, it's where you can add/remove  providers.
    - To add a new provider just add a new entry to this file, where you need to map the json file attributes and register the path of the json file of the data source, also you need to put the actual data source file into `/database/json`
    - visit `/api/v1/transactions/ to filter the result
    - allowed filters in the query string are (all are case insensitive):
        - currency
        - status (available values are: 'paid', 'pending' & 'reject')
        - amountMin
        - amountMax
        - provider
    and they can be combined together

    example: `http://you-server.test/api/v1/transactions?currency=usd&status=paid&amountMin=93`