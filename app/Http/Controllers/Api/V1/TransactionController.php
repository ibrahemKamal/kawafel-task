<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Libraries\DataProviderLiberary;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function __invoke(Request $request)
    {
        $providers = collect(config('data-providers'))
            ->reject(function ($config, $provider_name) use ($request) {
                return !empty($request->get('provider')) && $request->get('provider') != $provider_name;
            });
        $result = collect();

        $providers->each(function ($config) use (&$result, $request) {
            $provider = (new DataProviderLiberary)
                ->setConfig($config)
                ->setInitialData(DataProviderLiberary::fetchData($config))
                ->filterByStatus($request->get('status'))
                ->filterByCurrency($request->get('currency'))
                ->filterByAmount('>=', $request->get('amountMin'))
                ->filterByAmount('<=', $request->get('amountMax'))
            ;

            $result = $result->merge($provider->getData());
        });

        return response()->json([
            'data' => $result
        ]);
    }
}