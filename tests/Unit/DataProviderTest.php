<?php

namespace Tests\Unit;

use App\Libraries\DataProviderLiberary;
use Tests\TestCase;

class DataProviderTest extends TestCase
{
    /**
     * @test
     *
     * @return void
     */
    public function it_can_set_initial_data()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $this->assertCount(0, $provider->getData());

        // act
        $provider->setInitialData(DataProviderLiberary::fetchData(config('data-providers.DataProviderX')));

        // assert
        $this->greaterThan(0, $provider->getData());
    }

    /**
     * @test
     *
     * @return void
     */
    public function it_can_set_initial_data_with_array()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $data = [
            [
                "transactionAmount" => 200,
                "Currency" => "USD",
                "senderPhone" => "00201234567890",
                "transactionStatus" => 1,
                "transactionDate" => "2021-03-29 09:36:11",
                "transactionIdentification" => "d3d29d70-1d25-11e3-8591-034165a3a613"
            ]
        ];

        // act
        $provider->setInitialData($data);

        // assert
        $this->assertEquals(1, $provider->getData()->count());
        $this->assertEquals($data, $provider->getData()->toArray());
    }

    /**
     * @test
     *
     * @return void
     */
    public function it_can_set_initial_data_with_json_string()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $data = '[{"transactionAmount":200,"Currency":"USD","senderPhone":"00201234567890","transactionStatus":1,"transactionDate":"2021-03-29 09:36:11","transactionIdentification":"d3d29d70-1d25-11e3-8591-034165a3a613"}]';

        // act
        $provider->setInitialData($data);

        // assert
        $this->assertEquals(1, $provider->getData()->count());
        $this->assertEquals(json_decode($data, true), $provider->getData()->toArray());
    }

    /**
     * @test
     *
     * @return void
     */
    public function it_can_set_initial_data_with_json_string_with_data_key()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $data = '{"data":[{"transactionAmount":200,"Currency":"USD","senderPhone":"00201234567890","transactionStatus":1,"transactionDate":"2021-03-29 09:36:11","transactionIdentification":"d3d29d70-1d25-11e3-8591-034165a3a613"}]}';

        // act
        $provider->setInitialData($data);

        // assert
        $this->assertEquals(1, $provider->getData()->count());
        $this->assertEquals(json_decode($data, true)['data'], $provider->getData()->toArray());
    }


    /**
     * @test
     *
     * @return void
     */
    public function it_sets_malformed_json_into_empty_collection()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $data = 'WRONG_JSON_FORMAT';

        // act
        $provider->setInitialData($data);

        // assert
        $this->assertEquals(0, $provider->getData()->count());
    }

    /**
     * @test
     *
     * @return void
     */
    public function it_can_set_config()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $this->assertNull($provider->getConfig());

        // act
        $provider->setConfig(config('data-providers')['DataProviderX']);

        // assert
        $this->assertNotNull($provider->getConfig());
        $this->assertEquals(config('data-providers')['DataProviderX'], $provider->getConfig());
    }


    /**
     * @test
     *
     * @return void
     */
    public function it_can_be_filtered_by_currency()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $provider->setInitialData([
            ['Currency' => 'USD'],
            ['Currency' => 'EGP'],
            ['Currency' => 'USD'],
        ]);
        $provider->setConfig(config('data-providers')['DataProviderX']);
        $this->assertCount(3, $provider->getData());

        // act
        $provider->filterByCurrency('usd');

        // assert
        $this->assertCount(2, $provider->getData());
    }

    /**
     * @test
     *
     * @return void
     */
    public function it_can_be_filtered_by_status()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $provider->setInitialData([
            ['transactionStatus' => '1'],
            ['transactionStatus' => '2'],
            ['transactionStatus' => '3'],
        ]);
        $provider->setConfig(config('data-providers')['DataProviderX']);
        $this->assertCount(3, $provider->getData());

        // act
        $provider->filterByStatus('paid');

        // assert
        $this->assertCount(1, $provider->getData());
    }


    /**
     * @test
     *
     * @return void
     */
    public function it_can_be_filtered_by_amount_range()
    {
        // arrange
        $provider = new DataProviderLiberary;
        $provider->setInitialData([
            ['transactionAmount' => 1],
            ['transactionAmount' => 100],
            ['transactionAmount' => 200],
            ['transactionAmount' => 300],
            ['transactionAmount' => 500],
        ]);
        $provider->setConfig(config('data-providers')['DataProviderX']);
        $this->assertCount(5, $provider->getData());

        // act
        $provider->filterByAmount('>=', 0);
        $provider->filterByAmount('<=', 100);

        // assert
        $this->assertCount(2, $provider->getData());
    }
}
